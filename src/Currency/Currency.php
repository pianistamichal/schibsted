<?php
declare(strict_types=1);

namespace App\Currency;


use ReflectionClass;

abstract class Currency
{
    public const USD = "USD";
    public const JPY = "JPY";
    public const PLN = "PLN";
    public const CHF = "CHF";
    public const EUR = "EUR";
    public const CNY = "CNY";

    public static function getConstants(): array
    {
        $reflectionClass = new ReflectionClass(Currency::class);
        return $reflectionClass->getConstants();
    }
}