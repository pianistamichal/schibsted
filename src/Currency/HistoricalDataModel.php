<?php
declare(strict_types=1);

namespace App\Currency;

use JMS\Serializer\Annotation\Type;
use Nelmio\ApiDocBundle\Annotation\Model;

class HistoricalDataModel
{
    /**
     * @Type("DateTime<'Y-m-d'>")
     *
     * @var \DateTime
     */
    protected $date;

    /**
     * @Type("array<App\Currency\CurrencyValue>")
     *
     * @var CurrencyValue[]
     */
    protected $values;

    /**
     * HistoricalDataModel constructor.
     * @param \DateTime $date
     * @param CurrencyValue[] $values
     */
    public function __construct(\DateTime $date, array $values)
    {
        $this->date = $date;
        $this->values = $values;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @return CurrencyValue[]
     */
    public function getValues(): array
    {
        return $this->values;
    }
}