#Schibsted recruitment task

##Pre-requirements
Need php(working on 7.1) and composer(working on 1.2.1) to be installed on computer before any further steps.

##To install project

* Clone repository
* run `composer install`
* start server

##How to fast start server?
You have to run console command in main directory of project: `bin/console server:start`... and that's all!

##How to use
Make request through http client(e.g. postman) at some endpoint(e.g. http://127.0.0.1:8000/api/historicalData)

##To run tests

You have to run command: `./bin/phpunit`

##To run tests
There's also documentation. It can be found in http://localhost:8000/api/doc