<?php
declare(strict_types=1);

namespace App\Model;


use App\Currency\Currency;

class HistoricalDataFormModel
{
    /**
     * @var \DateTime|null
     */
    protected $startDate;

    /**
     * @var \DateTime|null
     */
    protected $endDate;

    /**
     * @var string|null
     */
    protected $baseCurrency;

    /**
     * @var array|null
     */
    protected $symbols;

    /**
     * HistoricalDataFormModel constructor.
     */
    public function __construct()
    {
        $this->startDate = new \DateTime("-1 year");
        $this->endDate = new \DateTime();
        $this->baseCurrency = Currency::USD;
        $this->symbols = [Currency::EUR, Currency::PLN];
    }

    /**
     * @return \DateTime|null
     */
    public function getStartDate(): ?\DateTime
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime|null $startDate
     */
    public function setStartDate(?\DateTime $startDate): void
    {
        $this->startDate = $startDate;
    }

    /**
     * @return \DateTime|null
     */
    public function getEndDate(): ?\DateTime
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime|null $endDate
     */
    public function setEndDate(?\DateTime $endDate): void
    {
        $this->endDate = $endDate;
    }

    /**
     * @return string|null
     */
    public function getBaseCurrency(): ?string
    {
        return $this->baseCurrency;
    }

    /**
     * @param string|null $baseCurrency
     */
    public function setBaseCurrency(?string $baseCurrency): void
    {
        $this->baseCurrency = $baseCurrency;
    }

    /**
     * @return array|null
     */
    public function getSymbols(): ?array
    {
        return $this->symbols;
    }

    /**
     * @param array|null $symbols
     */
    public function setSymbols( $symbols): void
    {
        $this->symbols = $symbols;
    }
}