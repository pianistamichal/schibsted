<?php
declare(strict_types=1);

namespace App\Currency\ExchangeRatesApi;

use App\Currency\CurrencyValue;
use App\Currency\HistoricalCurrency;
use App\Currency\HistoricalDataModel;
use App\Model\HistoricalDataFormModel;
use GuzzleHttp\ClientInterface;

class ERAHistoricalCurrency implements HistoricalCurrency
{
    /** @var ClientInterface */
    protected $httpClient;

    /**
     * ERAHistoricalCurrency constructor.
     * @param ClientInterface $guzzle
     */
    public function __construct(ClientInterface $guzzle)
    {
        $this->httpClient = $guzzle;
    }

    /**
     * {@inheritDoc}
     */
    public function get(HistoricalDataFormModel $historicalDataFormModel): array
    {
        $response = $this->httpClient->request("GET", "/history", [
            'query' => [
                'symbols' => implode(",", $historicalDataFormModel->getSymbols()),
                'start_at' => $historicalDataFormModel->getStartDate()->format("Y-m-d"),
                'end_at' => $historicalDataFormModel->getEndDate()->format("Y-m-d"),
                'base' => $historicalDataFormModel->getBaseCurrency()
            ]
        ])->getBody()->getContents();

        return $this->parseResponse($response);
    }

    /**
     * @param string $response
     * @return array
     * @throws \Exception
     */
    private function parseResponse(string $response): array
    {
        $jsonDecoded = json_decode($response, true);

        $toReturn = [];
        foreach ($jsonDecoded['rates'] as $date => $currencies) {
            $currenciesModels = [];
            foreach ($currencies as $currency => $price) {
                $currenciesModels[] = new CurrencyValue($currency, $price);
            }

            $toReturn[] = new HistoricalDataModel(new \DateTime($date), $currenciesModels);
        }

        return $toReturn;
    }
}