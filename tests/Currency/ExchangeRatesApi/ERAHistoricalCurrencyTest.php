<?php
declare(strict_types=1);

namespace App\Currency\ExchangeRatesApi;

use App\Currency\Currency;
use App\Currency\HistoricalDataModel;
use App\Model\HistoricalDataFormModel;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class ERAHistoricalCurrencyTest extends TestCase
{
    public const EXAMPLE_ERA_RESPONSE = "{\"base\":\"EUR\",\"rates\":{\"2018-08-02\":{\"PLN\":4.282},\"2018-08-01\":{\"PLN\":4.272}},\"end_at\":\"2018-08-02\",\"start_at\":\"2018-08-01\"}";
    public function testGetData(): void
    {
        $guzzleStream = $this->createMock(StreamInterface::class);
        $guzzleStream
            ->expects($this->once())
            ->method("getContents")
            ->willReturn(self::EXAMPLE_ERA_RESPONSE);
        $guzzleResponse = $this->createMock(ResponseInterface::class);
        $guzzleResponse
            ->expects($this->once())
            ->method("getBody")
            ->willReturn($guzzleStream);
        $guzzle = $this->createMock(ClientInterface::class);
        $guzzle
            ->expects($this->once())
            ->method("request")
            ->with("GET", "/history", [
                'query' => [
                    'symbols' => 'PLN',
                    'start_at' => '2018-08-01',
                    'end_at' => '2018-08-02',
                    'base' => 'EUR'
                ]
            ])
            ->willReturn($guzzleResponse);
        $eraHistoricalCurrency = new ERAHistoricalCurrency($guzzle);

        $historicalDataFormModel = new HistoricalDataFormModel();
        $historicalDataFormModel->setBaseCurrency(Currency::EUR);
        $historicalDataFormModel->setSymbols([Currency::PLN]);
        $historicalDataFormModel->setStartDate(new \DateTime('2018-08-01'));
        $historicalDataFormModel->setEndDate(new \DateTime('2018-08-02'));

        /** @var HistoricalDataModel[] $response */
        $response = $eraHistoricalCurrency->get($historicalDataFormModel);

        $dates = ["2018-08-01", "2018-08-02"];
        foreach ($response as $item) {
            $itemDateFormatted = $item->getDate()->format("Y-m-d");
            $this->assertContains($itemDateFormatted, $dates);
            $dates = array_diff($dates, [$itemDateFormatted]);

            $this->assertCount(1, $item->getValues());
            $this->assertSame(Currency::PLN, $item->getValues()[0]->getCurrency());
        }
    }
}