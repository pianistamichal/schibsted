<?php
declare(strict_types=1);

namespace App\Currency;

use App\Model\HistoricalDataFormModel;

interface HistoricalCurrency
{
    /**
     * @param HistoricalDataFormModel $historicalDataFormModel
     * @return HistoricalDataModel[]
     */
    public function get(HistoricalDataFormModel $historicalDataFormModel): array;
}