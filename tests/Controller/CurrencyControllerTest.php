<?php
declare(strict_types=1);

namespace App\Controller;

use App\Currency\Currency;
use App\Currency\CurrencyValue;
use App\Currency\HistoricalCurrency;
use App\Currency\HistoricalDataModel;
use App\Form\HistoricalDataForm;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CurrencyControllerTest extends TestCase
{
    public function testFormNotValid(): void
    {
        $form = $this->createMock(Form::class);
        $form
            ->expects($this->once())
            ->method("submit")
            ->willReturn($form);
        $form
            ->expects($this->once())
            ->method("isSubmitted")
            ->willReturn(true);
        $form
            ->expects($this->once())
            ->method("isValid")
            ->willReturn(false);

        $formFactory = $this->createMock(FormFactoryInterface::class);
        $formFactory
            ->expects($this->once())
            ->method("create")
            ->with(HistoricalDataForm::class)
            ->willReturn($form);

        $historicalCurrency = $this->createMock(HistoricalCurrency::class);

        $queryParameterBag = $this->createMock(ParameterBag::class);
        $queryParameterBag
            ->expects($this->once())
            ->method("all")
            ->willReturn(array());
        $request = $this->createMock(Request::class);
        $request->query = $queryParameterBag;
        $controller = new CurrencyController($historicalCurrency, $formFactory);
        $view = $controller->getHistoricalData($request);
        $this->assertSame(Response::HTTP_BAD_REQUEST, $view->getStatusCode());
    }

    public function testFormValid(): void
    {
        $form = $this->createMock(Form::class);
        $form
            ->expects($this->once())
            ->method("submit")
            ->willReturn($form);
        $form
            ->expects($this->once())
            ->method("isSubmitted")
            ->willReturn(true);
        $form
            ->expects($this->once())
            ->method("isValid")
            ->willReturn(true);

        $formFactory = $this->createMock(FormFactoryInterface::class);
        $formFactory
            ->expects($this->once())
            ->method("create")
            ->with(HistoricalDataForm::class)
            ->willReturn($form);

        $currencyValue = new CurrencyValue(Currency::PLN, 3.6);
        $historicalDataModel = new HistoricalDataModel(new \DateTime("2018-01-01"), [$currencyValue]);
        $historicalCurrency = $this->createMock(HistoricalCurrency::class);
        $historicalCurrency
            ->expects($this->once())
            ->method("get")
            ->willReturn([$historicalDataModel]);

        $queryParameterBag = $this->createMock(ParameterBag::class);
        $queryParameterBag
            ->expects($this->once())
            ->method("all")
            ->willReturn(array());
        $request = $this->createMock(Request::class);
        $request->query = $queryParameterBag;
        $controller = new CurrencyController($historicalCurrency, $formFactory);
        $view = $controller->getHistoricalData($request);
        $this->assertSame(Response::HTTP_OK, $view->getStatusCode());
        $this->assertSame([$historicalDataModel], $view->getData());
    }
}