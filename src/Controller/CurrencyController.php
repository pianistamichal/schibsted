<?php
declare(strict_types=1);

namespace App\Controller;

use App\Currency\HistoricalCurrency;
use App\Form\HistoricalDataForm;
use App\Model\HistoricalDataFormModel;
use FOS\RestBundle\View\View;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;

final class CurrencyController
{
    /** @var HistoricalCurrency */
    private $historicalCurrency;

    /** @var FormFactoryInterface */
    private $formFactory;

    /**
     * CurrencyController constructor.
     * @param HistoricalCurrency $historicalCurrency
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(HistoricalCurrency $historicalCurrency, FormFactoryInterface $formFactory)
    {
        $this->historicalCurrency = $historicalCurrency;
        $this->formFactory = $formFactory;
    }

    /**
     * @SWG\Parameter(
     *     name="startDate",
     *     in="query",
     *     type="string",
     *     description="Start date of currencies history"
     * )
     * @SWG\Parameter(
     *     name="endDate",
     *     in="query",
     *     type="string",
     *     description="End date of currencies history"
     * )
     * @SWG\Parameter(
     *     name="base",
     *     in="query",
     *     type="string",
     *     description="Base currency"
     * )
     * @SWG\Parameter(
     *     name="symbols",
     *     in="query",
     *     type="array",
     *     collectionFormat="multi",
     *     description="Array of curriencies to get data",
     *     @SWG\Items(
     *         type="string"
     *     )
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Returns the history of currencies",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=App\Currency\HistoricalDataModel::class))
     *     )
     * )
     * @Route("/historicalData", name="historical_data", methods={"GET"})
     *
     * @param Request $request
     * @return View
     * @throws \Exception
     */
    public function getHistoricalData(Request $request): View
    {
        $historicalDataFormModel = new HistoricalDataFormModel();
        $form = $this->formFactory->create(HistoricalDataForm::class, $historicalDataFormModel);
        $form->submit($request->query->all(), false);

        if($form->isSubmitted() && $form->isValid()) {
            return View::create($this->historicalCurrency->get($historicalDataFormModel), Response::HTTP_OK);
        }

        return View::create($form, Response::HTTP_BAD_REQUEST);
    }
}