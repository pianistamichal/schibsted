<?php
declare(strict_types=1);

namespace App\Currency;

use JMS\Serializer\Annotation\Type;

class CurrencyValue
{
    /**
     * @Type("string")
     *
     * @var string
     */
    protected $currency;

    /**
     * @Type("float")
     *
     * @var float
     */
    protected $relativePrice;

    /**
     * CurrencyValue constructor.
     * @param string $currency
     * @param float $relativePrice
     */
    public function __construct(string $currency, float $relativePrice)
    {
        $this->currency = $currency;
        $this->relativePrice = $relativePrice;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return float
     */
    public function getRelativePrice(): float
    {
        return $this->relativePrice;
    }
}